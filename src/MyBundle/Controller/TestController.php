<?php

namespace MyBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use MyBundle\Entity\test;
use MyBundle\Form\testType;
use FOS\RestBundle\Controller\Annotations\View;


class TestController extends Controller
{

    /**
     * Lists all test entities.
     *
     * @Route("/", name="test")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('MyBundle:test')->findAll();
        
        return $entities;

        /*return array(
            'entities' => $entities,
        );*/
    }
    
    
   
   
   public function getUserAction($id) {
     
        try {
            
             /*$em = $this->getDoctrine()->getManager();
            
            $q = Doctrine::getTable('login')->createQuery('u')->where('u.userName = ?', 'jwage');*/
            
             //$qb = $em->getRepository('MyBundle:test')->createQuery("SELECT COUNT(userName) FROM login ASC");
            
          /* $q = Doctrine_Query::create();
            ->select('a.userName')
            ->from('login a');

            echo $q->getSqlQuery();*/
            
           $em = $this->getDoctrine()->getManager ();
            $users = $em->getRepository ( 'MyBundle:test' )->findAll ();
            
            
            return $users;
        } catch ( Exception $e ) {
           
            return $e->getMessage ();
        }
    }
}
